<?php

namespace Drupal\measuremail\Plugin\Field\FieldFormatter;

use Drupal\Core\Entity\EntityFormBuilder;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldFormatter\EntityReferenceFormatterBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\measuremail\Entity\Measuremail;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'measuremail_form' formatter.
 *
 * @FieldFormatter(
 *   id = "measuremail_form",
 *   label = @Translation("Measuremail form"),
 *   field_types = {
 *     "entity_reference"
 *   }
 * )
 */
class MeasuremailFormFormatter extends EntityReferenceFormatterBase implements ContainerFactoryPluginInterface {

  /**
   * The renderer service.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * The entity form builder service.
   *
   * @var \Drupal\Core\Entity\EntityFormBuilder
   */
  protected $formBuilder;

  /**
   * Constructs an MeasuremailFormFormatter object.
   *
   * @param string $plugin_id
   *   The plugin_id for the formatter.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the formatter is associated.
   * @param array $settings
   *   The formatter settings.
   * @param string $label
   *   The formatter label display setting.
   * @param string $view_mode
   *   The view mode.
   * @param array $third_party_settings
   *   Any third party settings settings.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer service.
   * @param \Drupal\Core\Entity\EntityFormBuilder $form_builder
   *   The entity form builder service.
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, $label, $view_mode, array $third_party_settings, RendererInterface $renderer, EntityFormBuilder $form_builder) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);
    $this->renderer = $renderer;
    $this->formBuilder = $form_builder;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('renderer'),
      $container->get('entity.form_builder')
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function isApplicable(FieldDefinitionInterface $field_definition) {
    // This formatter is only available for entity types that reference
    // measuremail items.
    return ($field_definition->getFieldStorageDefinition()
        ->getSetting('target_type') == 'measuremail');
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];
    $measuremail_items = $this->getEntitiesToView($items, $langcode);

    // Early opt-out if the field is empty.
    if (empty($measuremail_items)) {
      return $elements;
    }

    /** @var \Drupal\measuremail\Entity\Measuremail[] $measuremail_items */
    foreach ($measuremail_items as $delta => $measuremail_item) {
      $elements[$delta] = $this->viewElement($measuremail_item, $langcode);

      // Add cacheability of each item in the field.
      $this->renderer->addCacheableDependency($elements[$delta], $measuremail_item);
    }

    return $elements;
  }

  /**
   * Render a single measuremail item as a form.
   *
   * @param \Drupal\measuremail\Entity\Measuremail $measuremail
   *   The measuremail entity.
   * @param $langcode
   *   The language that should be used to render the field.
   *
   * @return array
   *   A renderable array for the measuremail item.
   */
  public function viewElement(Measuremail $measuremail, $langcode) {
    $form = $this->formBuilder->getForm($measuremail, 'subscribe');

    // Add a class to allow more theming.
    $form['#attributes']['class'][] = 'form--measuremail--' . str_replace('_', '-', $measuremail->id());

    // We don't need the H1 title for a rendered measuremail form.
    unset($form['title']);

    return $form;
  }

}
